import '../css/main.scss'
import React, { Component } from 'react'
import { render } from 'react-dom'
import Skills from './Skills.js'


export default class App extends Component {
  constructor(props) {
    super(props)
  }


  render() {
    return (
      <div>
        <section id="skills">

          <article className='l-center-margins'>
            <h3>Click bars to show subchart</h3>
            <Skills />
          </article>
        </section>
      </div>
    )
  }
}

render(<App />, document.getElementById('app'))