import React, { Component } from 'react'
import { scaleBand, scaleLinear } from 'd3-scale'
import { range } from 'd3-array'
import Axes from './Axes.js'
import Bars from './Bars.js'

const margins = { top: 50, right: 20, bottom: 180, left: 60 }
export default class BarChart extends React.Component {
      constructor(props){
            super(props)
            this.xScale = scaleBand()
            this.yScale = scaleLinear()
            this.state = {size: {}}
            this.node
            this.el
            this.size = {width: 1024, height:768}
            this.barClick = this.barClick.bind(this)
      }
      
      barClick(barProps){
            if(this.props.barClick){
                  this.props.barClick(barProps)
            }
      }
      render() {
            let axesEl = ''
            const maxValue = 100    
            const xScale = this.xScale
            .padding(0.5)
            .domain(this.props.data.map(data => data.name))
            .range([margins.left, this.size.width - margins.right])       

            // scaleLinear type
            const yScale = this.yScale 
            .domain([0, maxValue])
            .range([this.size.height - margins.bottom, margins.top])       
            
            axesEl = <Axes scales={{ xScale:xScale, yScale:yScale }} margins={margins} 
            svgDimensions={this.size}/>
           
            return <div ref={ (el) => this.el = el} className="bar-chart-container l-col2" >
                  <svg ref={node => this.node = node} className={this.props.className} 
                  width='100%' viewBox="0 0 1024 768">
                  {axesEl}
                  {<Bars
                        scales={{ xScale:this.xScale, yScale:this.yScale }}
                        margins={margins}
                        data={this.props.data}
                        maxValue={maxValue}
                        svgDimensions={this.size}
                        barClick={this.barClick}
                        colorScale = {this.props.colorScale}
                  />}
                  </svg>
            </div>
      }
}