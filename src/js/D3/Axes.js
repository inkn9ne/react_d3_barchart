import React, { Component } from 'react'
import { render } from 'react-dom'
import ReactDOM from 'react-dom'
import { scaleBand, scaleLinear } from 'd3-scale'
import Axis from './Axis'

export default class Axes extends React.Component {
    constructor(props){
        super(props)
        this.xScale = this.props.scales.xScale
        this.yScale = this.props.scales.yScale
        this.size = this.props.svgDimensions

        this.xProps = {
            orient: 'Bottom',
            scale: this.props.scales.xScale,
            translate: `translate(0, ${this.props.svgDimensions.height - this.props.margins.bottom})`,
            tickSize: this.size.height - this.props.margins.top - this.props.margins.bottom
        }
    
        this.yProps = {
            orient: 'Left',
            scale: this.props.scales.yScale,
            translate: `translate(${this.props.margins.left}, 0)`,
            tickSize: this.size.width - this.props.margins.left - this.props.margins.right
        }
     }

    render() {
        this.xProps = {
            orient: 'Bottom',
            scale: this.props.scales.xScale,
            translate: `translate(0, ${this.props.svgDimensions.height - this.props.margins.bottom})`,
            tickSize: this.props.svgDimensions.height - this.props.margins.top - this.props.margins.bottom
        }
    
        this.yProps = {
            orient: 'Left',
            scale: this.props.scales.yScale,
            translate: `translate(${this.props.margins.left}, 0)`,
            tickSize: this.props.svgDimensions.width - this.props.margins.left - this.props.margins.right
        }

        return (
            <g>
                <Axis {...this.xProps} />
                <Axis {...this.yProps} />
            </g>
        )
    }
}