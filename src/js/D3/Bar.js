import React, { Component } from 'react'
import { transition } from 'd3-transition'
import { select } from 'd3-selection'

export default class Bar extends Component {
  constructor(props) {
    super(props)
    this.el
    this._fullDebug = false
    this.onRectClick = this.onRectClick.bind(this)
    this.barOpacity = 0
  }

  onRectClick(e) {
    this.props.onClick(this.props)
  }

  componentDidMount() {
    select(this.el)
      .transition()
      .attr('opacity', function (d) { return 1 })
      .attr('height', this.props.height)
      .attr('y', this.props.y)
      .duration(500)
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps !== this.props) {
      select(this.el)
        .transition()
        .attr('opacity', function (d) { return 1 })
        .attr('height', nextProps.height)
        .attr('width', nextProps.width)
        .attr('fill', nextProps.fill)
        .attr('y', nextProps.y)
        .attr('x', nextProps.x)
        .duration(500)
    }
    return false
  }

  render() {
    const { id, x, y, height, maxHeight, width, fill } = this.props
    return (
      <rect onClick={this.onRectClick}
        className='bar'
        key={this.key}
        ref={(el) => this.el = el}
        x={x}
        y={maxHeight}
        opacity={this.barOpacity}
        height={0}
        width={width}
        fill={fill}
      />
    )
  }
}