import React, { Component } from 'react'
import { scaleLinear } from 'd3-scale'
import Bar from './Bar.js'

export default class Bars extends Component {
  constructor(props) {
    super(props)
    this.barClick = this.barClick.bind(this)
    this.colorScale = this.props.colorScale
  }

  componentDidMount() {
    const { scales, margins, data, maxValue, svgDimensions } = this.props
    const { xScale, yScale } = scales
    const height = svgDimensions.height
  }

  barClick (barProps) {
    this.props.barClick(barProps)
  }

  render() {
    const { scales, margins, data, maxValue, svgDimensions, colorScale } = this.props
    const { xScale, yScale } = scales
    const height = svgDimensions.height
    let transform = `translate(${0}, ${0})`
    const bars = (
      data.map(data =>
        <Bar  
          key={data.id}
          id = {data.id}
          onClick = {this.barClick}
          x={xScale(data.name)}
          y={yScale(data.percentage)}
          height={height - margins.bottom - yScale(data.percentage)}
          maxHeight={height - margins.bottom}
          svgDimensions={svgDimensions}
          width={xScale.bandwidth()}
          fill={colorScale(data.percentage)}
        />
      )
    )

    return (
      <g ref={ (el) => this.el = el} transform={transform}>{bars}</g>
    )
  }
}