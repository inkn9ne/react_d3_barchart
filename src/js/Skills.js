import React, { Component } from 'react'
import { render } from 'react-dom'
import { scaleLinear } from 'd3-scale'
import BarChart from './D3/BarChart'
import '../json/skills.json'

export default class Skills extends React.Component {
  constructor(props) {
    super(props)
    this.skillsContainer
    this.state = { size: {} }
    this.colorScale = scaleLinear().domain([0, 100])
      .range(["red", "green", "blue", "yellow"])
    this.showSubChart = this.showSubChart.bind(this)
  }

  componentDidMount() {
    fetch('./json/skills.json',
      {
        headers: {
          'Accept': 'application/json',
        }
      })
      .then((response) => response.json())
      .then((response) => response.skills)
      .then((skillsJson) => {
        this.setState({ skills: skillsJson })
        this.onResize()
      })
      .catch(function (error) {
        console.log("error loading skills.json" + error)
      })
  }

  showSubChart(barProps) {
    let clickedSkill = this.state.skills[barProps.id - 1]
    this.setState({ subskills: clickedSkill.subskills })
    this.setState({ subBarChartColorScale: scaleLinear().domain([0, 100]).range([barProps.fill, "white"]) })
  }

  onResize() {
    if (this.skillsContainer) {
      let containerWidth = this.skillsContainer.getBoundingClientRect().width
      this.setState({ size: { width: containerWidth, height: containerWidth * 3 / 4 } })
    }
  }


  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.skills && this.state.skills != []) {
      return true
    } else {
      return false
    }
  }



  render() {
    var bChart, subChart = ""
    if (this.state.skills && this.state.skills != []) {
      bChart =
        <BarChart
          data={this.state.skills}
          className="bar-chart"
          colorScale={this.colorScale}
          barClick={this.showSubChart} />
    }
    return (
      <div id="skills-container" ref={(el) => this.skillsContainer = el}>
        {bChart}
        {this.state.subskills ? <BarChart
          data={this.state.subskills}
          className="bar-chart-sub"
          colorScale={this.state.subBarChartColorScale} /> : ''}
      </div>
    )
  }
}