# Webpack Babel React D3 #

Demo application with React and responsive d3 charts. 

![Scheme](src/img/screenshot.png)

React D3 barchart implementation. There are several techniques for implementing D3 in React. Ranging from doing everything in React, saving every animation step as state, over different combinations, to completely overtake the DOM with D3. This approach uses D3 calculations and helpers for the math and renders the view in React.
The BarChart components are using the svg viewbox attribute to automatically scale to fit the parent container.

Since React renders the child components before the parent ones, without using this attribute we would have to check if the parent containers are mounted and then pass the dimensions to the childs.

This is an approach which uses the souldComponentUpdate hook in BarComponent to give D3 the power of this portion of the DOM.


### Features ###

* Used d3 features are `d3-array`, `d3-axis`, `d3-ease`, `d3-scale`, `d3-selection`, `d3-transition`
* The Webpack configuration features Babel es2015 presets and also precompiles Scss files, has a dev-server configuration which recompiles if files are changed, and also a production build configuration with some optimization features. This project could be used as a basic configuration for such type of projects.


### How do I get set up? ###

* clone or download the repo
* make sure you have npm installed
* open console and cd to repo directory
* run `npm install`
* to run project on the local dev server run `npm run build`
* for production version run `npm run build:prod`